import os, sys
# Read the CSV file into a list and use ; as a delimiter
with open(sys.argv[1],"r") as f : list1 = (f.read().split('\n')[:-1])
for i in range(len(list1))      : list1[i] = list1[i].split(';')
# Get the indices of Status, state and the job occupations
head = list1[0]
index_certified     =   head.index([i for i in head if 'STATUS' in i][0])
index_occupation    =   head.index([i for i in head if 'SOC' in i and 'NAME' in i][0])
index_state         =   head.index([i for i in head if 'WORK' in i and 'STATE' in i][0])
# Processing the data and get the top ten occupations/states and filter the data in regards to the certified visas
list1.remove(head)
certified           =   list(filter(lambda x: x[index_certified] == 'CERTIFIED', list1))
for type in ['occupations', 'states']:
    index           =   index_occupation if type == 'occupations' else index_state
    category_raw    =   list(map(lambda x: x[index], certified)) #List of the occupations/states only if they are certified
    category        =   [i.replace('"', '') for i in category_raw]
    counts          =   dict()
    for i in category:
        counts[i]   =   counts.get(i, 0) + 1
    num_certified   =   len(certified)
# Calculated to get the percentage of the occupations out of all the certified occupations and write the list to a text file
    output_dir      =   sys.argv[1].replace('input','output').split(sys.argv[1].split('/')[-1])[0]
    filename        =   sys.argv[2] if type == 'occupations' else sys.argv[3]
    with open(filename, "w") as f :
        f.write('TOP_' + type.upper() + ';' + 'NUMBER_CERTIFIED_APPLICATIONS' + ';' + 'PERCENTAGE\n')
# Sort the counts dictionary with respect to key(alphabetical) with descending values and write the lists into text files
    sorted_counts   =   [val for val in sorted(counts.iteritems(), key=lambda(k, val): (-val, k))][:10]
    for i in sorted_counts:
        key         =   i[0]
        value       =   i[1]
        top_ten     =   ';'.join([key, str(value), str(round(value*100.0/num_certified,1))+ '%'])
        with open (filename,"a") as out:
            out.write("%s\n" % top_ten)
