Problem:

A newspaper editor was researching immigration data trends on H1B(H-1B, H-1B1, E-3) visa application processing over the past years, trying to identify the occupations and states with the most number of approved H1B visas. She has found statistics available from the US Department of Labor and its Office of Foreign Labor Certification Performance Data. But while there are ready-made reports for 2018 and 2017, the site doesn’t have them for past years.
As a data engineer, you are asked to create a mechanism to analyze past years data, specifically calculate two metrics: Top 10 Occupations and Top 10 States for certified visa applications.
Your code should be modular and reusable for future. If the newspaper gets data for the year 2019 (with the assumption that the necessary data to calculate the metrics are available) and puts it in the input directory, running the run.sh script should produce the results in the output folder without needing to change the code.

Approach:

The problem is solved using Python. Both Python2 and Python3 can be used. No external libraries and/or modules are used. The approach is as follows:
1. The cvs file is read into a list, and the header is written to another list.
2. Keywords are used to find if an entry is CERTIFIED, and also for the OCCUPATION and the STATE.
3. Steps 4 through 6 for each of: OCCUPATIONS and STATES
4. Filter the list with respect to the certified applicants.
5. An empty dictionary is initialized and and it consists of key-value pairs of the type and the count of the type of output. 
6. Write the type of the list, along with number and percentage of certified applicants.


Run Instructions:

1. Go to the working directory ./insight using the command cd ./insight
2. Run the shell script using the command ./run.sh
3. Check the output folder for the text files
